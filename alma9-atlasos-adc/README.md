AlmaLinux OS 9 for ATLAS
=============

This configuration can be used to set up a base image that ATLAS offline releases can be installed on top of. So that each image would not have to duplicate the same base components.

During the build you can, and should choose the GCC version to be installed,
explicitly.

You can build a GCC 13 based aarch64 image with the following:

```bash
podman build --platform linux/arm64 -t registry.cern.ch/atlas-sit/alma9-atlasos:gcc13-aarch64  --build-arg GCCVERSION=13.1.0 --build-arg BINUTILSVERSION=2.40 --compress --squash .
```

You can build a GCC 13 based x86_64 image with the following:

```bash
podman build --platform linux/amd64 -t registry.cern.ch/atlas-sit/alma9-atlasos:gcc13-x86_64  --build-arg GCCVERSION=13.1.0 --build-arg BINUTILSVERSION=2.40 --compress --squash .
```

You can build a multi-architecture manifest with the following:

```bash
podman manifest create registry.cern.ch/atlas-sit/alma9-atlasos:gcc13-multi registry.cern.ch/atlas-sit/alma9-atlasos:gcc13-x86_64 registry.cern.ch/atlas-sit/alma9-atlasos:gcc13-aarch64
```

Images
------

You can find pre-built images at
[registry.cern.ch/atlas-sit/alma9-atlasos](https://registry.cern.ch/harbor/projects/3671/repositories/alma9-atlasos)
