# Copyright (C) 2002-2024 CERN for the benefit of the ATLAS collaboration
#
# Environment setup for CMake 3.29.5.
#

export PATH=/opt/cmake/3.29.5/Linux-x86_64/bin${PATH:+:${PATH}}
