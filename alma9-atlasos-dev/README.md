AlmaLinux OS 9 for ATLAS Software Development
=================================

This image adds libraries on top of `alma9-atlasos`, which are necessary for
a "full offline software development environment".

You can build the image with the following:

```bash
docker build \
   -t gitlab-registry.cern.ch/atlas-sit/docker/alma9-atlasos-dev:X.Y.Z-gcc13 \
   -t gitlab-registry.cern.ch/atlas-sit/docker/alma9-atlasos-dev:X.Y.Z \
   -t gitlab-registry.cern.ch/atlas-sit/docker/alma9-atlasos-dev:latest-gcc13 \
   -t gitlab-registry.cern.ch/atlas-sit/docker/alma9-atlasos-dev:latest \
   --compress .
```

Images
------

You can find pre-built images on
[gitlab-registry.cern.ch/atlas-sit/docker/alma9-atlasos-dev](https://gitlab.cern.ch/atlas-sit/docker/container_registry/17862).
