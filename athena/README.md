Base Images for the Athena, AthSimulation and AtlasOffline Projects.
======================================

This procedure can be used to build an image providing a full
installation of an Athena, AthSimulation or AtlasOffline release.

Building images with podman
----
Build an image for a numbered release in the AthSimulation project in an aarch64 host:

```bash
podman build --platform linux/arm64 -t registry.cern.ch/atlas/athsimulation:24.0.42-aarch64 --build-arg BASEIMAGE=registry.cern.ch/atlas-sit/alma9-atlasos:gcc13-aarch64 --build-arg RELEASE=24.0.42 --build-arg PROJECT=AthSimulation --build-arg BINARY_TAG=aarch64-el9-gcc13-opt --compress --squash .
```

Build an image for a nightly release in the AthSimulation project in an aarch64 host:

```bash
podman build --platform linux/arm64 -t registry.cern.ch/atlas/athsimulation:24.0.42-2024-06-25T0001-aarch64 --build-arg BASEIMAGE=registry.cern.ch/atlas-sit/alma9-atlasos:gcc13-aarch64 --build-arg RELEASE=24.0.42 --build-arg PROJECT=AthSimulation --build-arg BINARY_TAG=aarch64-el9-gcc13-opt --build-arg TIMESTAMP=2024-06-25T0001 --compress --squash .
```

Build an image for a numbered release in the AthSimulation project in a x86_64 host:
```bash
podman build --platform linux/amd64 -t registry.cern.ch/atlas/athsimulation:24.0.42-x86_64 --build-arg BASEIMAGE=registry.cern.ch/atlas-sit/alma9-atlasos:gcc13-x86_64 --build-arg RELEASE=24.0.42 --build-arg PROJECT=AthSimulation --build-arg BINARY_TAG=x86_64-el9-gcc13-opt --compress --squash .
```

Build a multi-architecture manifest for a numbered release in the AthSimulation project:
```bash
podman manifest create registry.cern.ch/atlas/athsimulation:24.0.42 registry.cern.ch/atlas/athsimulation:24.0.42-x86_64 registry.cern.ch/atlas/athsimulation:24.0.42-aarch64
```

Images
--------

You can find pre-built images at
[registry.cern.ch/atlas/athsimulation](https://registry.cern.ch/harbor/projects/225/repositories/athsimulation)
