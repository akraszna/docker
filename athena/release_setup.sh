# Copyright (C) 2002-2024 CERN for the benefit of the ATLAS collaboration
#
# Environment configuration file for the user. Can be sourced to set up
# the release installed in the image.
#

USEDBRELEASE=true
while [ True ]; do
if [ "$1" = "--use-frontier" -o "$1" = "-u" ]; then
    USEDBRELEASE=false
    shift 1
else
    break
fi
done

export INSTALLATION_ROOT=/usr
export SITEROOT=${INSTALLATION_ROOT}
export G4PATH=/usr/Geant4
export VO_ATLAS_SW_DIR=/usr
export CALIBPATH=/usr/database/GroupData
export ATLAS_RELEASEDATA=/usr/atlas/offline/ReleaseData

for project in \
        "Athena" \
        "AthSimulation" \
        "AtlasOffline"
do
    if [ -d "/usr/$project" ]; then
       PROJECT=$project
       RELEASE=$(\cd /usr/$project/;\ls)
       BINARY_TAG=$(\cd /usr/$project/${RELEASE}/InstallArea/;\ls)
    fi
done

export LCG_PLATFORM=${BINARY_TAG}

# Set up the compiler:
source /opt/lcg/gcc/*/setup.sh
echo "Configured GCC from: ${CC}"

if [ -d "/usr/database/DBRelease" ]; then
   export ATLAS_DB_AREA=/usr/database
fi

# LCG:
export LCG_RELEASE_BASE=/opt/lcg
echo "Taking LCG releases from: ${LCG_RELEASE_BASE}"

# Set up the release:
source ${INSTALLATION_ROOT}/${PROJECT}/${RELEASE}/InstallArea/${BINARY_TAG}/setup.sh
echo "Configured ${PROJECT} from: ${INSTALLATION_ROOT}/${PROJECT}/${RELEASE}/InstallArea/${BINARY_TAG}"

# Include patches
PATCH_PATH=/opt/build/${BINARY_TAG}
PATCH_SETUP=${PATCH_PATH}/setup.sh
if [ -f "$PATCH_SETUP" ]; then
  echo "Including local patches at " $PATCH_PATH
  source $PATCH_SETUP
fi

if [ "$USEDBRELEASE" = true ]; then
  unset FRONTIER_SERVER
  echo Setting up DBRelease /usr/database/DBRelease/current environment
  export DBRELEASE=current
  export CORAL_AUTH_PATH=/usr/database/DBRelease/current/XMLConfig
  export CORAL_DBLOOKUP_PATH=/usr/database/DBRelease/current/XMLConfig
  export TNS_ADMIN=/usr/database/DBRelease/current/oracle-admin
  DATAPATH=/usr/database/DBRelease/current:$DATAPATH
fi

