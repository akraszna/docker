# Copyright (C) 2002-2024 CERN for the benefit of the ATLAS collaboration
#
# Environment configuration file for the ATLAS user. Can be sourced to set up
# the analysis release installed in the image.
#

# Execute all of the environment setup scripts of the image/OS.
for script in /etc/profile.d/*.sh; do
   source ${script}
done

# Set up a nice prompt:
export PS1='\[\033[01;35m\][bash]\[\033[01;31m\][\u Athena-$Athena_VERSION]\[\033[01;34m\]:\W >\[\033[00m\] ';

# Set up the compiler:
source /opt/lcg/binutils/*/x86_64-*/setup.sh
source /opt/lcg/gcc/*/x86_64-*/setup.sh
echo "Configured GCC from: ${CC}"

# Set up (the rest of) LCG:
export LCG_RELEASE_BASE=/opt/lcg
echo "Taking LCG releases from: ${LCG_RELEASE_BASE}"

# Set up Gaudi, if necessary. If the /usr/GAUDI directory doesn't exist, then
# just assume that it's a "new release" in which Gaudi is part of
# AthenaExternals.
if [ -d "/usr/GAUDI" ]; then
   GAUDI_VERSION=$(\cd /usr/GAUDI/;\ls)
   GAUDI_PLATFORM=$(\cd /usr/GAUDI/${GAUDI_VERSION}/InstallArea/;\ls)
   export GAUDI_ROOT=/usr/GAUDI/${GAUDI_VERSION}/InstallArea/${GAUDI_PLATFORM}
   unset GAUDI_VERSION
   unset GAUDI_PLATFORM
   echo "Taking Gaudi from: ${GAUDI_ROOT}"
fi

# Configure Calibration via HTTP
export PATHRESOLVER_ALLOWHTTPDOWNLOAD=1

# Set up the analysis release:
source /usr/Athena/*/InstallArea/*/setup.sh
echo "Configured Athena from: ${Athena_DIR}"
