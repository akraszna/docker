#!/bin/sh
project_sanity_test=''
[[ "${PROJECT_SANITY_TEST}" != "" ]] && project_sanity_test="${PROJECT_SANITY_TEST}" 
while [ $# -ne 0 ]; do
    case $1 in
         --project) project_sanity_test=$2; shift;;
         -* | --*)         echo "image_build_and_push_to_harbor.sh: Error: invalid option"; exit 1;;
    esac
    shift
done
if [ "${project_sanity_test}" = "" ]; then
    echo "container_sanity_test: Error: project is not defined neither in the enviroment nor in command line"
    exit 1
fi    
echo "====================================================================="
echo "===This is the logfile of a ${project_sanity_test} container sanity test"
if [ "${project_sanity_test}" != "AthAnalysis" -a "${project_sanity_test}" != "AnalysisBase" ]; then
echo "===It includes simple checks: AthExHelloWorld/HelloWorldOptions"
echo "===-------------------------- AthExStoreGateExample/StoreGateExample_Gen_jobOptions.py"
echo "===-------------------------- Tools/PyUtils/test/test_RootUtils.py"
echo "===-------------------------- G4AtlasApps/test/test_AtlasG4_tf_configuration.py"
echo "====================================================================="
elif [ "${project_sanity_test}" = "AthAnalysis" ]; then    
echo "===It includes simple checks: "
echo "===-------------------------- Tools/PyUtils/test/test_RootUtils.py"
echo "===-------------------------- EgammaAnalysisAlgorithms/EgammaAnalysisAlgorithmsTest_jobOptions.py"
echo "===-------------------------- SUSYTools/minimalExampleJobOptions_mc.py" 
echo "====================================================================="
fi
source /release_setup.sh
echo "===RUNTIME ENVIRONMENT" 
printenv
echo "===RUNTIME ENVIRONMENT printout end"

[[ -d /tmp/container_tests_workdir ]] && rm -rf /tmp/container_tests_workdir
mkdir -p /tmp/container_tests_workdir
cd /tmp/container_tests_workdir

if [ "${project_sanity_test}" = "AnalysisBase" ]; then
    st_HelloWorldOptions=0
    st_PyUtils=0
    st_StoreGateExample=0
    st_G4=0
    st_EGAlg=0
    st_SUSY=0
    echo "===SKIPPING tests for project ${project_sanity_test}"
elif [ "${project_sanity_test}" = "AthAnalysis" ]; then
    st_HelloWorldOptions=0
    st_G4=0
    st_StoreGateExample=0
    echo "===STARTING Tools/PyUtils test"
    echo "\$${project_sanity_test}_DIR/src/Tools/PyUtils/test/test_RootUtils.py"
    eval python "\$${project_sanity_test}_DIR/src/Tools/PyUtils/test/test_RootUtils.py"; st_PyUtils=$?
    [ "$st_PyUtils" -eq 0 ]] && echo "===Test Tools/PyUtils PASSED" || echo "===Test Tools/PyUtils FAILED $st_PyUtils"
    echo "===STARTING PhysicsAnalysis/Algorithms/EgammaAnalysisAlgorithms  test"
    athena.py EgammaAnalysisAlgorithms/EgammaAnalysisAlgorithmsTest_jobOptions.py - --data-type data --old-file PROPERTIES TIMEOUT 600
    st_EGAlg=$?
    [[ "$st_EGAlg" -eq 0 ]] && echo "===Test EgammaAnalysisAlgorithms PASSED" || echo "===Test EgammaAnalysisAlgorithms FAILED $st_EGAlg"
    echo "===STARTING SUSYTools/minimalExample test"
    athena SUSYTools/minimalExampleJobOptions_mc.py --evtMax=1 -c 'MCCampaign="MCa"'; st_SUSY=$?
    [[ "$st_SUSY" -eq 0 ]] && echo "===Test SUSYTools/minimalExample PASSED" || echo "===Test SUSYTools/minimalExample FAILED $st_SUSY"    
else
    st_EGAlg=0
    st_SUSY=0				 
    echo "===STARTING AthExHelloWorld/HelloWorldOptions test"
    athena.py AthExHelloWorld/HelloWorldOptions.py; st_HelloWorldOptions=$?
    [[ "$st_HelloWorldOptions" -eq 0 ]] && echo "===Test HelloWorldOptions PASSED" || echo "===Test HelloWorldOptions FAILED HelloWorldOptions"
    if [ "${project_sanity_test}" = "Athena" ]; then
      echo "===STARTING StoreGateExample test"
      athena.py AthExStoreGateExample/StoreGateExample_Gen_jobOptions.py; st_StoreGateExample=$?
      [[ "$st_StoreGateExample" -eq 0 ]] && echo "===Test StoreGateExample PASSED" || echo "===Test StoreGateExample FAILED ${st_StoreGateExample}"
    else
      echo "===SKIPPING StoreGateExample test for project ${project_sanity_test}"
      st_StoreGateExample=0	
    fi
    echo "===STARTING Tools/PyUtils test"
    eval python "\$${project_sanity_test}_DIR/src/Tools/PyUtils/test/test_RootUtils.py" ; st_PyUtils=$?
    [[ "$st_PyUtils" -eq 0 ]] && echo "===Test Tools/PyUtils PASSED" || echo "===Test Tools/PyUtils FAILED $st_PyUtils"
    echo "===STARTING AtlasG4_tf_configuration test"
    eval python "\$${project_sanity_test}_DIR/src/Simulation/G4Atlas/G4AtlasApps/test/test_AtlasG4_tf_configuration.py" ; st_G4=$?
    [[ "$st_G4" -eq 0 ]] && echo "===Test AtlasG4_tf_configuration PASSED" || echo "===Test xAOD AtlasG4_tf_configuration FAILED $st_G4"
fi
if [ "$st_StoreGateExample" -ne 0 -o "$st_PyUtils" -ne 0 -o "$st_G4" -ne 0 -o "$st_HelloWorldOptions" -ne 0 -o "$st_EGAlg" -ne 0 -o "$st_SUSY" -ne 0 ]; then
echo "Container sanity test: error:  the following tests failed:"
[[ "$st_StoreGateExample" -ne 0 ]] && echo "--- StoreGateExample failed with exit code $st_HelloWorldConfig"
[[ "$st_PyUtils" -ne 0 ]] &&	echo "--- PyUtils failed with exit code $st_PyUtils"
[[ "$st_G4" -ne 0 ]] &&	echo "--- G4 failed with exit code $st_G4"
[[ "$st_HelloWorldOptions" -ne 0 ]] &&	echo "--- HelloWorldOptions failed with exit code $st_HelloWorldOptions"
[[ "$st_EGAlg" -ne 0 ]] &&    echo "--- EGAlg failed with exit code $st_EGAlg"
[[ "$st_SUSY" -ne 0 ]] &&    echo "--- SUSY failed with exit code $st_SUSY"
exit 1
else
echo "Container sanity test PASSED"
exit 0
fi


    
