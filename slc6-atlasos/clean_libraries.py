#!/usr/bin/env python
#
# Script used to post-process the SFT GCC RPM.
#

# Take the name of the directory to process, from the command line.
import sys
if len( sys.argv ) != 2:
   print( 'Usage: clean_libraries.py <directory>' )
   sys.exit( 1 )
   pass
directory = sys.argv[ 1 ]

# First off, remove all of the "*~" files.
import os,glob
outdatedFiles = glob.glob( os.path.join( directory, '*~' ) )
for f in outdatedFiles:
   os.remove( f )
   pass

# Now update all of the deep copies of the shared libraries to symbolic links.
import re
libraryFiles = glob.glob( os.path.join( directory, '*.so' ) )
for f in libraryFiles:
   # Find "all versions" of this library.
   libs = glob.glob( '%s.*' % f )
   # Construct a regular expression pattern that all library names should
   # match. This is to avoid picking up files that have a name starting the
   # same as one of the libraries. Of which are a few in the GCC installation.
   libPattern = re.compile( '^%s\.[0-9\.]*[0-9]$' %
                            f.replace( '.', '\.' ).replace( '+', '\+' ) )
   # Find the "main library" (with the longest name), and the ones that
   # need to be re-created as symbolic links.
   mainLib = ''
   reCreateLibs = [ f ]
   for name in libs:
      if not libPattern.match( name ):
         continue
      if len( name ) > len( mainLib ):
         mainLib = name
         pass
      reCreateLibs.append( name )
      pass
   reCreateLibs.remove( mainLib )
   # Get the file name of the main library, to create the symbolic links
   # with relative path names.
   mainLib = os.path.basename( mainLib )
   # Now re-create them as symbolic links.
   for lib in reCreateLibs:
      os.remove( lib )
      os.symlink( mainLib, lib )
      pass
   pass

# Finally update all references to /cvmfs/sft.cern.ch in the *.la files.
laFiles = glob.glob( os.path.join( directory, '*.la' ) )
for f in laFiles:
   os.system( 'sed -i.bak -e "s:/cvmfs/sft.cern.ch/lcg/contrib:/opt/lcg:g" %s' % f )
   os.remove( '%s.bak' % f )
   pass
